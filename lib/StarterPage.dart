import 'package:flutter/material.dart';
import 'package:homework4/FadeAnimation.dart';
import 'package:homework4/main.dart';

import 'package:page_transition/page_transition.dart';

class StarterPage extends StatefulWidget {
  @override
  _StarterPageState createState() => _StarterPageState();
}

class _StarterPageState extends State<StarterPage> with TickerProviderStateMixin{
  AnimationController _animationController;
  Animation<double> _animation;

  bool _textVisible = true;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100)
    );

    _animation = Tween<double>(
      begin: 1.0,
      end: 25.0
    ).animate(_animationController);

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();

    super.dispose();
  }

  void _onTap() {
    setState(() {
      _textVisible = false;
    });



  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/starter-image.jpg'),
            fit: BoxFit.cover
          )
        ),
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomCenter,
              colors: [
                Colors.black.withOpacity(.9),
                Colors.black.withOpacity(.8),
                Colors.black.withOpacity(.2),
              ]
            )
          ),
          child: Padding(
            padding: EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FadeAnimation(.5, Text('Entry fees stating at 24.99', style: TextStyle(color: Colors.white, fontSize: 50, fontWeight: FontWeight.bold),)),
                SizedBox(height: 20,),
                FadeAnimation(1, Text("Contact us for this fun experience @ \n(560)-773-3465", style: TextStyle(color: Colors.white, height: 1.4, fontSize: 18),)),
                SizedBox(height: 100,),
                FadeAnimation(1.2, 
                ScaleTransition(
                  scale: _animation,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      gradient: LinearGradient(
                        colors: [
                          Colors.yellow,
                          Colors.orange
                        ]
                      )
                    ),
                  )),

                ),
                Container(
                    margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child:
                    RaisedButton(
                      onPressed: () =>
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) => new MyApp())),
                      child: Text('Back'),
                      textColor: Colors.white,
                      color: Colors.red,
                      padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
                    )
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}