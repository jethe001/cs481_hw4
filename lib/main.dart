import 'package:flutter/material.dart';
import 'package:homework4/StarterPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(

        home: Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              title: Text('Casino'),
              centerTitle: true,
              backgroundColor: Colors.red,
              elevation: 0,
            ),
            body: Center(
                child: RotateImage()
            )
        )
    );
  }
}

class RotateImage extends StatefulWidget {
  @override
  RotateImageState createState() => new RotateImageState();
}

class RotateImageState extends State
    with SingleTickerProviderStateMixin {

  AnimationController animationController;

  @override
  void initState() {
    super.initState();

    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 5),
    );

    animationController.repeat();
  }

  stopRotation(){

    animationController.stop();
  }

  startRotation(){

    animationController.repeat();
  }


  @override
  Widget build(BuildContext context) {
    return Column(

        mainAxisAlignment: MainAxisAlignment.center,

        children: <Widget>[

          Container(
              color: Colors.white,
              alignment: Alignment.center,
              child: AnimatedBuilder(
                animation: animationController,
                child: Container(
                  height: 350.0,
                  width: 350.0,
                  child: Image.asset('assets/images/cir.png',
                    width: 350, height: 350, fit: BoxFit.contain,),
                ),
                builder: (BuildContext context, Widget _widget) {
                  return Transform.rotate(
                    angle: animationController.value * 6.3,
                    child: _widget,
                  );
                },
              )),

          Container(
              margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child:
              RaisedButton(
                onPressed: () => stopRotation(),
                child: Text(' Stop Wheel'),
                textColor: Colors.white,
                color: Colors.red,
                padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
              )
          ),
          Container(
              margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child:
              RaisedButton(
                onPressed: () => startRotation(),
                child: Text('Start Wheel'),
                textColor: Colors.white,
                color: Colors.red,
                padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
              )
          ),
          Container(
              margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              child:
              RaisedButton(
                onPressed: () =>
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => new StarterPage())),
                child: Text('Get More Infomation Here  !!'),
                textColor: Colors.white,
                color: Colors.blue,
                padding: EdgeInsets.fromLTRB(12, 12, 12, 12),
              )
          ),
        ]);
  }
}
